/*
* 04/14/21
* The purpose of this class is to retrieve and display current weather data
*/

var temp_history = [];
var hum_history = [];

writeHistory();

// const fs = require('fs')
//
// fs.readFile('temp.txt', 'utf-8', (err, data) => {
//     if (err) throw err;
//
//     var currentTemp = parseFloat(data);
//     temp_history.push(currentTemp);
//     document.getElementById('0_data-1_day').textContent=currentTemp;
// })
//
// fs.readFile('hum.txt', 'utf-8', (err, data) => {
//     if (err) throw err;
//
//     var currentHum = parseFloat(data);
//     temp_history.push(currentHum);
//     document.getElementById('0_data-2_day').textContent=currentHum;
// })


function writeHistory(){


  fetch('h_temp.txt')
  .then(response=>response.text())
  .then(text=> {
    var lines = text.split('\n');
    var days = ["S","M", "T", "W", "Th", "F", "S"];
    for(i=1; i<6; i++){
	//day = days[(Date.getDate()-i).getDay()]
	today = new Date();
	d = new Date();
	d.setDate(today.getDate()-i);
	day = days[d.getDay()];
	//console.log(d.getDate()-i);
	text = lines[lines.length-i-1];
	day = "<span style=\"text-align: left;\">"+day+"</span>";
    	document.getElementById(i+'_day-1_data').innerHTML = "<span>"+text+"</span>";
	document.getElementById(i+'_day-0_data').innerHTML = day;
    }
  });

  fetch('h_hum.txt')
  .then(response=>response.text())
  .then(text=> {
    var lines = text.split('\n');
	for(i=1; i<6; i++){
	text = lines[lines.length-i-1];
    	document.getElementById(i+'_day-2_data').textContent=text;
    }
  });

  fetch('h_ws.txt')
  .then(response=>response.text())
  .then(text=> {
    var lines = text.split('\n');
	for(i=1; i<6; i++){
	text = lines[lines.length-i-1];
	document.getElementById(i+'_day-3_data').textContent=text;
    }
  });
}

