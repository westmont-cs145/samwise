/*
* 04/07/21
* The purpose of this class is to retrieve and display historical weather data
*/

var temp_history = [];
var hum_history = [];

writeCurrent();

// const fs = require('fs')
//
// fs.readFile('temp.txt', 'utf-8', (err, data) => {
//     if (err) throw err;
//
//     var currentTemp = parseFloat(data);
//     temp_history.push(currentTemp);
//     document.getElementById('0_data-1_day').textContent=currentTemp;
// })
//
// fs.readFile('hum.txt', 'utf-8', (err, data) => {
//     if (err) throw err;
//
//     var currentHum = parseFloat(data);
//     temp_history.push(currentHum);
//     document.getElementById('0_data-2_day').textContent=currentHum;
// })

function writeCurrent(){

 var innerHTML;

  fetch('c_temp.txt')
  .then(response=>response.text())
  .then(text=> {
    document.getElementById('0_day-1_data').textContent=text+'°';
  });

  fetch('c_hum.txt')
  .then(response=>response.text())
  .then(text=> {
      document.getElementById('0_day-2_data').textContent=text+'%';
  });

  fetch('c_ws.txt')
  .then(response=>response.text())
  .then(text=> {
      document.getElementById('0_day-3_data').innerHTML = "<p style=\"margin-bottom: 0px; font-size: 70%;\">"+text+"mph \r\n</p>";
 }); 

  fetch('c_dir.txt')
   .then(response=>response.text())
   .then(text=> {
      if(text){
      	document.getElementById('0_day-3_data').innerHTML+= "<p style=\"font-size:40%;\">wind direction: "+text+"°</p>";
      }

  });

  setTimeout(writeCurrent, 10000);
}

