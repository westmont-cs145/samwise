# 04/7/21
# The purpose of this class is to read serial data and store in files accessible to web server 


import serial
import time
import datetime;
import math

#store data throughout day in array
tempHistory = [];
humHistory = [];
wsHistory = [];


checked = 0;
time.sleep(2)

#establish serial port and baud rate
ser = serial.Serial('/dev/ttyUSB0', 9600)

#at regular intervals, find interval high and lows and write to history files
def sendDataToFile():

    #record humidity high and low
    low = 1000;
    high = -1000;
    for hum in humHistory:
        hum = float(hum);
        if(hum < low):
            low = hum;
        if(hum > high):
            high = hum;
        data = 'Low '+str(low)+'% / High '+str(high)+'%\n';
    f = open('h_hum.txt', 'a')
    f.write(data)
    f.close()

    #record temperate high and low
    low = 1000;
    high = -1000;

    for temp in tempHistory:
        temp = float(temp);
        if(temp < low):
            low = temp;
        if(temp > high):
            high = temp;
        data = 'Low '+str(low)+' /  High '+str(high)+'\n';
    f = open('h_temp.txt', 'a')
    f.write(data)
    f.close()

    #record windspeed high
    data = None
    high = -1000;
    for ws in wsHistory:
        ws = float(ws);
        if(ws > high):
            high = ws;
        data = "High: "+str(high)+'mph'+'\n';
    if(data):
        f = open('h_ws.txt', 'a')
        f.write(data)
        f.close()



#monitor time in order to update historical data at midnight
def checkTime():
    global tempHistory
    global humHistory
    global wsHistory
    now = datetime.datetime.now()
    #if(now.hour == 0 and checked < 0):
    if(len(tempHistory) == 5):
       sendDataToFile();
       tempHistory = [];
       humHistory = [];
       wsHistory = [];
       checked = 360;


counter = 0
atsix = False
timer = 0
while True:
    counter += 1;
    # Put hum data in temp file
    b = ser.readline()
    string_n = b.decode()
    string = string_n.rstrip()
    print(string)
    if counter%10 == 0:
        humHistory.append(string);
        f = open('c_hum.txt', 'w');
        f.write(string);
        f.close();

    # Put temp data in hum file
    b = ser.readline()
    string_n = b.decode()
    string = string_n.rstrip()
    string = str(((float(string)*9)/5)+32);
    print(string);
    if counter%10 == 0:
        tempHistory.append(string);
        f = open('c_temp.txt', 'w');
        f.write(string);
        f.close();
        checkTime();
        counter = 0;
    #time.sleep(1);
    checked -= 1;
    #checkTime();

    # print distance
    b = ser.readline()
    print(b)
    if int(b)==6 and atsix==False:
        atsix = True
        newtime = time.time();
        if timer==0:
           timer = newtime
        else:
            string = str((15*math.pi/(newtime - timer))/17.6);
            string = string[0:4]
            wsHistory.append(string)
            print(string);
            f = open('c_ws.txt', 'w');
            f.write(string);
            f.close();
            timer = newtime
    elif int(b) > 9 and atsix==True:
        atsix = False



    b = ser.readline()
    coords = b.split(",")
    x = float(coords[0])
    y = float(coords[1])
    if(x!=0):
        string = str(math.atan(x/y)*180/3.1415)
        string = string[0:5]
        print(string)
        f = open('c_dir.txt', 'w');
        f.write(string);
        f.close();
ser.close()   



