/**
 * Sharp IR sensor uses SharpIR library
 * https://github.com/guillaume-rico/SharpIR
 * 
 * Example based on https://www.makerguides.com/sharp-gp2y0a21yk0f-ir-distance-sensor-arduino-tutorial/
 *
 *
  */




// Include libraries:
#include <SharpIR.h>
#include "DHT.h"        // including the library of DHT11 temperature and humidity sensor

#define DHTTYPE DHT11   // DHT 11
#define dht_dpin 0

// Define sensor model GP2Y0A21YK0F and input pin:
#define IRPin A0
#define model 1080

#include <Adafruit_LSM303DLH_Mag.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>

Adafruit_LSM303DLH_Mag_Unified mag = Adafruit_LSM303DLH_Mag_Unified(12345);
void displaySensorDetails(void) {
  sensor_t sensor;
  mag.getSensor(&sensor);
  
  /*Serial.println("------------------------------------");
  Serial.print("Sensor:       ");
  Serial.println(sensor.name);
  Serial.print("Driver Ver:   ");
  Serial.println(sensor.version);
  Serial.print("Unique ID:    ");
  Serial.println(sensor.sensor_id);
  Serial.print("Max Value:    ");
  Serial.print(sensor.max_value);
  Serial.println(" uT");
  Serial.print("Min Value:    ");
  Serial.print(sensor.min_value);
  Serial.println(" uT");
  Serial.print("Resolution:   ");
  Serial.print(sensor.resolution);
  Serial.println(" uT");
  Serial.println("------------------------------------");
  Serial.println("");*/
  delay(500);
}

// Create a new instance of the SharpIR class:
SharpIR mySensor = SharpIR(IRPin, model);

DHT dht(dht_dpin, DHTTYPE); 
void setup(void)
{ 
  dht.begin();
  Serial.begin(9600);
  //Serial.println("Humidity and temperature\n\n");
  Wire.begin();

  //Serial.println("Magnetometer Test");
  //Serial.println("");
 
  /* Enable auto-gain */
  //mag.enableAutoRange(true);

  //mag.begin();
  /* Initialise the sensor */
  //if (!mag.begin()) {
    /* There was a problem detecting the LSM303 ... check your connections */
    //Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    //while (1)
      //;
  //}
  
 
  /* Display some basic information on this sensor */
  //displaySensorDetails();

 

}
void loop() {
    float h = dht.readHumidity();
    float t = dht.readTemperature();
    Serial.println(h);
    Serial.println(t);
    unsigned int distance = mySensor.distance(); // read distance from sensor
    Serial.println(distance);

  mag.begin();
      /* Get a new sensor event */
  sensors_event_t event;
  mag.getEvent(&event);
 
  /* Display the results (magnetic vector values are in micro-Tesla (uT)) */
  //Serial.print("X: ");
  Serial.print(event.magnetic.x);
  Serial.print(",");
  //Serial.print("Y: ");
  Serial.println(event.magnetic.y);
  //Serial.print("  ");
  //Serial.print("Z: ");
  //Serial.print(event.magnetic.z);
  //Serial.print("  ");
  //Serial.println("uT");
 
  /* Delay before the next sample */
    
    
    delay(50);
}
