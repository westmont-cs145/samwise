This is the Weather Station project written on Ruby, the best Raspberry Pi in the world. 
In this file you will learn all you need to know about our project, and the people who made it.


First, head over to this link in order to see the weather station website:
http://10.90.14.150/


Once there, you can see the current date and time in Santa Barbara, California. This is the location of our Raspberry
Pi, Ruby, and we obtain our weather data from this location. This was written with Python, Javascript, and on Arduino


There are three columns on our website including temperature, humidity, as well as wind speed and direction

The temperature column measures the temperature in fahrenheit
The humidity column measures the humidity in percent form
The wind speed and direction measures the wind speed in miles per hour and direction in degrees from north

These columns measure the data from previous days, with the top row being the current weather, and the rows below
being the previous days highs/lows.



The weather station website is only online when we turn Ruby on, so make sure to contact us if you want to see it for
yourself with the days current weather.


The members of team Ruby are:
Payton Dugas:Team Leader/Coder/Hardware
Talia Bjelland:Big Coder, Owner and Namer of Ruby
Jack Chiplin:Hardware and troubleshooting



For the project we used the following libraries:
Adafruit_BusIO
Adafruit_LSM303DLH_Mag
Adafruit_Sensor-master
Average-master
DHT_sensor_library
LSM303
SharpIR
serial
time
datetime
math
