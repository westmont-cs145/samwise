/*
* 04/7/21
* The purpose of this class is to provide structure for displaying weather data
*/

var col = 4;
var row = 6;

var months = ['January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

buildTable();
getTimeAndDate();

function buildTable(){
  var html = '';
  for(let i = 0; i < row; i++){
    html += '<tr>';
    for(let j = 0; j < col; j++){
      var id = i.toString()+"_day-"+j.toString()+'_data';
      if(i == 0){
	html += '<td id='+id+' class="row1" style="border: 1px solid rgb(220,220,220);"'+'></td>';
      } else {
      	html += '<td id='+id+' style="border: 1px solid rgb(220,220,220);"'+'></td>';i
      }
    }
    html += '</tr>';
  }
  $('#tb_body').html(html);
}

function getTimeAndDate(){
  var html = '';
  var d = new Date();
  var year = d.getFullYear();
  var month = d.getMonth();
  var day = d.getDate();
  var hour = d.getHours();
  var minute = d.getMinutes();
  if(minute > 0 && minute < 10)
	minute = '0'+minute;
  var suffix = (hour > 12) ? 'pm': 'am';
  hour = hour%12;
  if(hour == 0){
     hour = 12;
  }
  html += months[month]+' '+day+getSuffix(day)+', '+year+'&nbsp; &nbsp;'+hour+':'+minute+suffix;

  $('#time_date').html(html);
}

function getSuffix(d){
  if (d==1 || d==21 || d==31){
    return 'st'
  }
  if (d==2 || d==22){
      return 'nd'
  }
  if (d==3 || d==23){
      return 'rd'
  }
  else{
      return 'th'
  }
}

